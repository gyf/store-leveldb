VERSION230=v2.3.0

gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION230)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION230s)
	go mod tidy

lint:
	golangci-lint run ./...

ut:
	go test -v ./...
